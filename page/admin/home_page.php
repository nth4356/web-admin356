<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // require "model\user/control_page.php";
    require "config/conn.php";
    require "model/user/function.php";
    ?>
    <div class="body">
        <div class="left">
            LEFT
        </div>
        <div class="right">
            <br>
            <div class="title">
                <div class="left-title">
                    <span>MAIN PAGE</span>
                </div>
                <div class="right-title">
                    Home /
                    <?php
                    if (isset($_GET["id"])) {
                        echo ucfirst($_GET["id"]);
                    } else {
                        echo "Banner";
                    }
                    ?>
                </div>
            </div>
            <div class="main-right">
                <div class="button-main">
                    <a href="index.php?id=banner">
                        <div class="button">Banner</div>
                    </a>
                    <a href="index.php?id=category">
                        <div class="button">Category</div>
                    </a>
                    <a href="index.php?id=slider">
                        <div class="button">Slider</div>
                    </a>
                    <a href="index.php?id=products">
                        <div class="button">Products</div>
                    </a>
                </div>
                <div class="insert">
                    <?php
                    if (isset($_GET["id"])) {
                        require "model\user/control_page.php";
                    }
                    // require "model\user/home_page.php";
                    ?>
                </div>
                <div class="list">
                    <?php
                    $query = select_bannerimg($conn); ?>
                    <th>
                        <table>
                            <tr>
                                <th>URL Banner</th>
                                <th>URL Web</th>
                            </tr>
                            <?php
                            while ($row = mysqli_fetch_array($query)) {
                                ?>
                                <tr>
                                    <td><?php echo $row[1];?></td>
                                    <td><?php echo $row[2];?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </th>
                </div>
            </div>
        </div>
    </div>




    <!-- <div class="home_page">
        <div class="title">Main</div>
        <div class="selection">
            <a href="index.php?id=banner">
                <div class="button">Banner</div>
            </a>
            <a href="index.php?id=category">
                <div class="button">Category</div>
            </a>
            <a href="index.php?id=slider">
                <div class="button">Slider</div>
            </a>
            <a href="index.php?id=products">
                <div class="button">Products</div>
            </a>
        </div>
        <div>

        </div>
    </div> -->
</body>

</html>