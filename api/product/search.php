<?php 

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");
error_reporting(E_ERROR);
if ($_SERVER['REQUEST_METHOD'] !== 'GET') :
    http_response_code(405);
    echo json_encode([
        'success' => 0,
        'message' => 'Bad Reqeust Detected! Only get method is allowed',
        
    ]);
    exit;
endif;
require "../../config/conn.php";
$name = '';
if(isset($_GET['name'])){
    $name = $_GET['name'];
}
$sql ="SELECT * FROM products inner join category on category.id_category  like products.id_category WHERE products.name like '%$name%' or category.name_type like '%$name%' ";
$result = $conn->query($sql);
$response_array['search_products']=[];
if ($result->num_rows > 0) {
    header('Content-Type:application/json');
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        array_push($response_array['search_products'], $row);
    }
    echo json_encode($response_array, JSON_PRETTY_PRINT);
} else {
    echo "0 results";
   
}
$conn->close();

?>



