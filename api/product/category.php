<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");
require"../../config/conn.php";
$sql="SELECT * FROM `category`";
$result=mysqli_query($conn,$sql);
$response_array['category']=[];
if ($result->num_rows > 0) {
    header('Content-Type:application/json');
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        array_push($response_array['category'], $row);
    }
    echo json_encode($response_array, JSON_PRETTY_PRINT);
} else {
    echo "0 results";
}
$conn->close();

?>

