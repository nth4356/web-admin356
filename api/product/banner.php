<?php
require "../../config/conn.php";
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");
error_reporting(E_ERROR);
if ($_SERVER['REQUEST_METHOD'] !== 'GET') :
    http_response_code(405);
    echo json_encode([
        'success' => 0,
        'message' => 'Bad Reqeust Detected! Only get method is allowed',
        
    ]);
    exit;
endif;

$id_banner = null;
if(isset($_GET['id_banner'])){
    $id_banner = filter_var($_GET['id_banner'],
    FILTER_VALIDATE_INT, [
        'options' => [
            'default' => 'all_images',
            'min_range' => 1
        ]
]);
}
$sql =is_numeric($id_banner)? "SELECT * FROM bannerimg WHERE id_banner = '$id_banner'":"SELECT * FROM `bannerimg`";
$result = mysqli_query($conn,$sql);
$response_array['banner']=[];
if ($result->num_rows > 0) {
    header('Content-Type:application/json');
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        array_push($response_array['banner'], $row);
    }
    echo json_encode($response_array, JSON_PRETTY_PRINT);
} else {
    echo "0 results";
}
$conn->close();

?>