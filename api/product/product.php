<?php 
require "../../config/conn.php";
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");
error_reporting(E_ERROR);
if ($_SERVER['REQUEST_METHOD'] !== 'GET') :
    http_response_code(405);
    echo json_encode([
        'success' => 0,
        'message' => 'Bad Reqeust Detected! Only get method is allowed',
        
    ]);
    exit;
endif;

$id = null;
$idCategory=null;
if(isset($_GET['id'])){
    $id = filter_var($_GET['id'],
    FILTER_VALIDATE_INT, [
        'options' => [
            'default' => 'all_product',
            'min_range' => 1
        ]
]);


}
$sql =is_numeric($id)? "SELECT * FROM products WHERE id = '$id'":"SELECT * FROM `products`";
$result = $conn->query($sql);
$response_array['products']=[];
if ($result->num_rows > 0) {
    header('Content-Type:application/json');
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        array_push($response_array['products'], $row);
    }
    echo json_encode($response_array, JSON_PRETTY_PRINT);
} else {
    echo "0 results";
}
$conn->close();

?>



