<?php
require "../../config/conn.php";
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");
error_reporting(E_ERROR);
if ($_SERVER['REQUEST_METHOD'] !== 'GET') :
    http_response_code(405);
    echo json_encode([
        'success' => 0,
        'message' => 'Bad Reqeust Detected! Only get method is allowed',
        
    ]);
    exit;
endif;

$id_category = null;
if(isset($_GET['id_category'])){
    $id_category = filter_var($_GET['id_category'],
    FILTER_VALIDATE_INT, [
        'options' => [
            'default' => 'all_images',
            'min_range' => 1
        ]
]);
}
$sql =is_numeric($id_category)? "SELECT * FROM category inner join products on products.id_category like category.id_category WHERE products.id_category=
'$id_category'":"SELECT * FROM category inner join products on products.id_category like category.id_category";
$result = $conn->query($sql);
$response_array['products_category']=[];
if ($result->num_rows > 0) {
    header('Content-Type:application/json');
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        array_push($response_array['products_category'], $row);
    }
    echo json_encode($response_array, JSON_PRETTY_PRINT);
} else {
    echo "0 results";
}
$conn->close();

?>